package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPlant is a Querydsl query type for Plant
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlant extends EntityPathBase<Plant> {

    private static final long serialVersionUID = 71878168L;

    public static final QPlant plant = new QPlant("plant");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public QPlant(String variable) {
        super(Plant.class, forVariable(variable));
    }

    public QPlant(Path<? extends Plant> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlant(PathMetadata<?> metadata) {
        super(Plant.class, metadata);
    }

}

