package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPlantInventoryEntry is a Querydsl query type for PlantInventoryEntry
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlantInventoryEntry extends EntityPathBase<PlantInventoryEntry> {

    private static final long serialVersionUID = -941326162L;

    public static final QPlantInventoryEntry plantInventoryEntry = new QPlantInventoryEntry("plantInventoryEntry");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public QPlantInventoryEntry(String variable) {
        super(PlantInventoryEntry.class, forVariable(variable));
    }

    public QPlantInventoryEntry(Path<? extends PlantInventoryEntry> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlantInventoryEntry(PathMetadata<?> metadata) {
        super(PlantInventoryEntry.class, metadata);
    }

}

