package com.example.models;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPlantReservation is a Querydsl query type for PlantReservation
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPlantReservation extends EntityPathBase<PlantReservation> {

    private static final long serialVersionUID = 595977876L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlantReservation plantReservation = new QPlantReservation("plantReservation");

    public final QBusinessPeriod rentalPeriod;

    public QPlantReservation(String variable) {
        this(PlantReservation.class, forVariable(variable), INITS);
    }

    public QPlantReservation(Path<? extends PlantReservation> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantReservation(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlantReservation(PathMetadata<?> metadata, PathInits inits) {
        this(PlantReservation.class, metadata, inits);
    }

    public QPlantReservation(Class<? extends PlantReservation> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.rentalPeriod = inits.isInitialized("rentalPeriod") ? new QBusinessPeriod(forProperty("rentalPeriod")) : null;
    }

}

