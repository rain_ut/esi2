package com.example.models;

import com.example.Springtest2Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

/**
 * Created by ytt on 2/19/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Springtest2Application.class)
@Sql(scripts="plant-dataset.sql")
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantCatalogTests {
    @Autowired
    PlantInventoryEntryRepository plantRepo;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantRepo.count(), is(14l));

    }

    @Test
    public void queryByName() {
        System.out.println(plantRepo.findByNameContaining("Mini"));
        assertThat(plantRepo.findByNameContaining("Mini").size(), is(2));
    }


    @Test
    public void findAvailableTest() {
        PlantInventoryEntry p = plantRepo.findOne(2l);
//        System.out.println(p);
    System.out.println(plantRepo.findAvailablePlants(LocalDate.of(2016,03,22),LocalDate.of(2016,03,24)));
       // ArrayList<PlantInventoryEntry>v=plantRepo.findAvailablePlants(LocalDate.of(2016,03,22),LocalDate.of(2016,03,24));

     // assertThat(plantRepo.findAvailablePlants(LocalDate.of(2016,03,22),LocalDate.of(2016,03,24)),hasItem(p));
//
       PurchaseOrder po = new PurchaseOrder();
//        po.setPlant(p);
//        po.setRentalPeriod(BusinessPeriod.of(LocalDate.of(2016, 2, 20), LocalDate.of(2016, 2, 25)));
//        poRepo.save(po);
//
//        assertThat(plantRepo.findAvailablePlants(LocalDate.of(2016,2,20), LocalDate.of(2016,2,25)), not(hasItem(p)));
    }

}