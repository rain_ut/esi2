package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by ytt on 2/23/2016.
 */
@Entity
@Data
public class MaintenanceTask {
    @Id
    @GeneratedValue
    Long id;
    String description;
    @Enumerated(EnumType.STRING)
    TypeOfWork type_of_work;
    @Column(precision=8,scale=2)
    BigDecimal price;
    @Embedded
    BusinessPeriod mainatancePeriod;




}
