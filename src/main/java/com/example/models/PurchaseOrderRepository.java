package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by ytt on 2/23/2016.
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long>, QueryDslPredicateExecutor<PurchaseOrder> {



}
