package com.example.models;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
public class PurchaseOrder {
  @Id @GeneratedValue
  Long id;

 List<PlantReservation> reservations;
 @OneToOne
  PlantInventoryEntry plant;

  LocalDate issueDate;
  LocalDate paymentSchedule;
  @Column(precision=8,scale=2)
  BigDecimal total;

  @Enumerated(EnumType.STRING)
  POStatus status;
    @Embedded
    BusinessPeriod rentalPeriod;
}
