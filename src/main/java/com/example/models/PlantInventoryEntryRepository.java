package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import  com.example.models.PlantInventoryEntry;
import  com.example.models.PlantInventoryEntry;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long>, QueryDslPredicateExecutor<PlantInventoryEntry> {
    List<PlantInventoryEntry> findByNameContaining(String str);
   @Query("select p from PlantInventoryEntry p where LOWER(p.name) like ?1")
   List<PlantInventoryEntry> finderMethod(String name);
    @Query(value = "select po from PurchaseOrder po where po.startDate =:start AND po.endDate =:enddate")
    List<Map.Entry<PlantInventoryEntry, Integer>>findAvailablePlants(@Param("start") LocalDate start, @Param("enddate") LocalDate enddate);
}

