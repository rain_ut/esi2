package com.example.models;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * Created by ytt on 2/23/2016.
 */
@Entity
@Data
public class MaintenancePlan {
    @OneToMany(cascade={CascadeType.ALL})
    List<MaintenanceTask> maintenanceTasks;

    @OneToOne
    PlantInventoryEntry plantInventoryEntry;

    int year_of_action;
// TODO zero to one realtionship
}
