package com.example.models;

/**
 * Created by ytt on 2/23/2016.
 */
public enum  EquipmentCondition {
    Serviceable,
    UnserviceableRepairable,
    UnserviceableIncomplete,
    UnserviceableCondemned
}
