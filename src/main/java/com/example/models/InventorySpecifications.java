package com.example.models;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.types.expr.BooleanExpression;

public class InventorySpecifications {
    static QPlantInventoryEntry plant = QPlantInventoryEntry.plantInventoryEntry;
    static QPlantReservation reservation = QPlantReservation.plantReservation;
    static QPurchaseOrder purchaseOrder = QPurchaseOrder.purchaseOrder;
//    public static BooleanExpression isAvailableFor(BusinessPeriod period) {
//        return plant.notIn(
//                new JPASubQuery().from(purchaseOrder)
//                        .where(purchaseOrder.rentalPeriod.startDate=period.startDate
//        )
//        .list(reservation.plant));
//    }

    public static BooleanExpression nameContains(String keyword) {
        return plant.name.lower().contains(keyword.toLowerCase());
    }
}