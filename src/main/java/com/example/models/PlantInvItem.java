package com.example.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;


@Entity
@Data
public class PlantInvItem {
    @ManyToOne
    PlantInventoryEntry plantInventoryEntry;
    String serial_number;
    @Enumerated(EnumType.STRING)
    EquipmentCondition equipment_condition;

}
