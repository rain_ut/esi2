package com.example.models;

import lombok.Data;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by ytt on 2/23/2016.
 */
@Entity
@Data
public class PlantReservation {
    @Embedded
    BusinessPeriod rentalPeriod;


}
